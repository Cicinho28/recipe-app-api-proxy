# Recipe-app-api-proxy

NGINX proxy app for our recipe app API


## usage

### Environment variables

* "LISTEN_PORT" (default: "8000")
* "APP_HOST" (default: "app")
* "APP_PORT" (default: "9000")